package week1.day04;

import java.util.*;

public class CollectionExercises {
    public static void main(String[] args) {

        System.out.println("Ex 19");
        List<String> cities = new ArrayList<>();
        cities.add("Washington");
        cities.add("Moscow");
        cities.add("Tokyo");
        cities.add("Dublin");
        cities.add("Stockholm");
        System.out.println(cities);
        System.out.println("Esimene linn: " + cities.get(0));
        System.out.println("Kolmas linn: " + cities.get(2));
        System.out.println("Viimane linn: " + cities.get(cities.size() - 1));

        System.out.println("Ex 20");
        Queue<String> countries = new LinkedList<>();
        countries.add("Eesti");
        countries.add("Saksamaa");
        countries.add("Norra");
        countries.add("Suurbritannia");
        countries.add("Hispaania");
        System.out.println("Kas queue on tühi? " + countries.isEmpty());

        // while, countries.isEmpty() countries.poll();
        while (!countries.isEmpty()) {
            System.out.println(countries.poll());
        }

        System.out.println("Ex 21");
        Set<String> countriesSet = new TreeSet<>();
        countriesSet.add("Eesti");
        countriesSet.add("Saksamaa");
        countriesSet.add("Norra");
        countriesSet.add("Suurbritannia");
        countriesSet.add("Hispaania");

        countriesSet.forEach(c -> System.out.println(c));

//        for (String myCountry : countriesSet) {
//            System.out.println(myCountry);
//        }

        System.out.println("Ex 22");
        Map<String, String[]> countriesMap = new HashMap<>();
        countriesMap.put("Estonia", new String[]{"Tallinn", "Tartu", "Valga", "Võru"});
        countriesMap.put("Sweden", new String[]{"Stockholm", "Uppsala", "Lund", "Köping"});
        countriesMap.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});
        System.out.println(countriesMap);

//        String[] keys = countriesMap.keySet().toArray(String[]::new);
//        for(int i = 0; i < keys.length; i++) {
//            String myCurrentCountryName = keys[i];
//            System.out.println("Country: " + myCurrentCountryName);
//            System.out.println("Cities:");
//            String[] myCurrentCities = countriesMap.get(myCurrentCountryName);
//
//            for(int j = 0; j < myCurrentCities.length; j++) {
//                System.out.println("\t" + myCurrentCities[j]);
//            }
//        }

        for(String countryName : countriesMap.keySet()) {
            System.out.println("Country:" + countryName);
            System.out.println("Cities:");
            for (String city : countriesMap.get(countryName)) {
                System.out.println("\t" + city);
            }
        }
    }
}
