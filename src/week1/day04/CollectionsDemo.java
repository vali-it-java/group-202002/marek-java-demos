package week1.day04;

import java.util.*;

public class CollectionsDemo {
    public static void main(String[] args) {

        // ---------------------
        // List, ArrayList
        // ---------------------

        // Massiiv täisarvudest
        int[] myArray = { 1, 3, 8, 9, 8};
        myArray[2] = 89;
        System.out.println(myArray[2]);
        System.out.println(myArray);

        // List täisarvudest
        List<Integer> myList = new ArrayList<>();
        // NB: Kollektsioonide juures EI SAA KASUTADA "[" ja "]" märke.
        myList.add(1);
        myList.add(3);
        myList.add(8);
        myList.add(9);
        myList.add(8);
        System.out.println(myList);
        System.out.println("Kolmas listi element: " + myList.get(2));

        // Elementide kustutamine listist
        // Variant 1
        myList.remove(1);
        System.out.println(myList);

        // Variant 2
        myList.remove((Integer)1);
        System.out.println(myList);

        // Küsime listilt tema elemendi kohta
        System.out.println("Kas sa sisaldad numbrit 8? " + myList.contains(8));
        System.out.println("Kas sa sisaldad numbrit 2? " + myList.contains(2));

        // Ütle mulle selle elemendi indeks
        System.out.println("Elemendi 8 indeks: " + myList.indexOf(8));
        System.out.println("Elemendi 2 indeks: " + myList.indexOf(2));

        System.out.println("Listi pikkus: " + myList.size());

        // Olemasoleva elemendi asendamine listis...
        myList.set(0, 5);
        System.out.println(myList);

        // Elemendi vahele asetamine...
        myList.add(1, 3);
        System.out.println(myList);

        // ---------------------
        // Set, HashSet, TreeSet
        // ---------------------

        Set<String> names = new TreeSet<>();
        names.add("Malle");
        names.add("Mai");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Meelis");
        names.add("Mihkel");
        names.add("Toomas");
        names.add("Rainer");
        names.add("Ahti");
        System.out.println(names);

        // Setti saab läbi käia foreach tsükliga, aga mitte for-tsükliga, sest
        // indeksit pole kuhgi panna (.get() meetod puudub).
        for(String name : names) {
            System.out.println(name);
        }

        // Kui tahan kasutada tavalist for tsüklit, pean seti konverteerima massiiviks.
        String[] namesArray = names.toArray(String[]::new);
        for(int i = 0; i < namesArray.length; i++) {
            System.out.println(namesArray[i]);
        }

        // Alatas Java 8 - uus ja äge!
        names.forEach(name -> System.out.println(name));

        // ---------------------
        // Map, HashMap
        // ---------------------

        Map<String, String> estonianEnglishDictionary = new HashMap<>();
        estonianEnglishDictionary.put("auto", "car");
        estonianEnglishDictionary.put("maja", "house");
        estonianEnglishDictionary.put("ratas", "wheel");
        System.out.println(estonianEnglishDictionary);
        System.out.println("Maja inglise keeles? " + estonianEnglishDictionary.get("maja"));
        System.out.println("Kõik võtmed...");
        System.out.println(estonianEnglishDictionary.keySet());

        for(String key : estonianEnglishDictionary.keySet()) {
            System.out.println("Väärtusele " + key + " vastab sõna " +
                    estonianEnglishDictionary.get(key));
        }
    }
}
