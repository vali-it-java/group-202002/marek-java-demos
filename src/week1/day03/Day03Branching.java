package week1.day03;

public class Day03Branching {

    public static void main(String[] args) {
        int personAge = 17;
        if(personAge >= 18) {
            System.out.println("Person is adult");
            System.out.println("So somehting else");
        } else {
            System.out.println("Person is not adult");
        }

        int temperature = 25;
        if (temperature >= 25) {
            System.out.println("On soe suveilm");
        } else if (temperature >= 20) {
            System.out.println("On tavaline suveilm");
        } else if (temperature >= 15) {
            System.out.println("On jaanipäev");
        } else {
            System.out.println("Ilm ei ole ilus.");
        }

        // Variant 1: Inline method
        String isEven = Integer.parseInt(args[0]) % 2 == 0 ?
                "The number is even" : "The number is odd";
        System.out.println(isEven);

        // Variant 2: "classic" method
        if (Integer.parseInt(args[0]) % 2 == 0) {
            isEven = "The number is even";
        } else {
            isEven = "The number is odd";
        }
        System.out.println(isEven);

        // Variant 1 and Variant 2 are semantically same.
    }
}
