package week1.day03;

public class LoopExercises {
    public static void main(String[] args) {

        System.out.println("Ex 11");
        int counter = 1;
        while (counter <= 100) {
            System.out.println(counter);
            counter++;
        }

        System.out.println("Ex 12");
        for (int i = 1; i < 101; i++) {
            System.out.println(i);
        }

        System.out.println("Ex 13");
        int[] myNumbersArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int myNumber : myNumbersArray) {
            System.out.println(myNumber);
        }

        System.out.println("Ex 14");
        // Variant 1
        for (int i = 3; i <= 100; i = i + 3) {
            System.out.println(i);
        }

        // Variant 2
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("Ex 15");
        // Lõppeesmärk: bandsText == "Sun, Metsatöll, Queen, Metallica"
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
        String bandsText = "";

        // Variant 1
        for (int i = 0; i < bands.length; i++) {
            bandsText = bandsText + bands[i];
            if (i < bands.length - 1) {
                bandsText = bandsText + ", ";
            }
        }

        System.out.println(bandsText);

        // Variant 2
        System.out.println(String.join(", ", bands));

        System.out.println("Ex 16");
        bandsText = "";
        for (int i = bands.length - 1; i >= 0; i--) {
            bandsText = bandsText + bands[i];
            if (i > 0) {
                bandsText = bandsText + ", ";
            }
        }

        System.out.println("Bändid tagant pool ettepoole:");
        System.out.println(bandsText);

        System.out.println("Ex 17");
        String textToPrint = "";
        // args[0] == "5" ==> prindime: "viis"
        // 5 ==> "viis"
        String[] words = {
                "null", "üks", "kaks",
                "kolm", "neli", "viis",
                "kuus", "seitse", "kaheksa",
                "üheksa"
        };

        for (int i = 0; i < args.length; i++) {
            int number = Integer.parseInt(args[i]);
//        words[0] ==> "null"
//        words[1] ==> "üks"
            textToPrint = textToPrint + words[number];
            if (i < args.length - 1) {
                textToPrint += ", ";
            } else {
                textToPrint += ".";
            }
        }

        System.out.println("Numbrid sõnadega:");
        System.out.println(textToPrint);

        System.out.println("Ex 18");
        double myRandNum;
        do {
            System.out.println("Tere");
            myRandNum = Math.random();
            System.out.println(myRandNum);
        } while(myRandNum < 0.5);

    }
}
