package week1.day03;

public class ArrayExercises {
    public static void main(String[] args) {
        System.out.println("Ex 7");
        int[] myNumbers;
        myNumbers = new int[5];
        myNumbers[0] = 1;
        myNumbers[1] = 2;
        myNumbers[2] = 3;
        myNumbers[3] = 4;
        myNumbers[4] = 5;
        System.out.println("Esimene element: " + myNumbers[0]);
        System.out.println("Kolmas element: " + myNumbers[2]);
        System.out.println("Viimane element: " + myNumbers[myNumbers.length - 1]);

        System.out.println("Ex 8");
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        System.out.println("Minu viimane linn: " + cities[cities.length - 1]);

        System.out.println("Ex 9");
        int[][] myCoolNumbers = new int[3][];
        myCoolNumbers[0] = new int[3];
        myCoolNumbers[1] = new int[3];
        myCoolNumbers[2] = new int[4];

        // Anname massiile väärtused...

        // Rida 1...
        myCoolNumbers[0][0] = 1;
        myCoolNumbers[0][1] = 2;
        myCoolNumbers[0][2] = 3;

        // Rida 2...
        myCoolNumbers[1][0] = 4;
        myCoolNumbers[1][1] = 5;
        myCoolNumbers[1][2] = 6;

        // Rida 3...
        myCoolNumbers[2][0] = 7;
        myCoolNumbers[2][1] = 8;
        myCoolNumbers[2][2] = 9;
        myCoolNumbers[2][3] = 0;

        System.out.println("Teise rea viimane element: " + myCoolNumbers[1][2]);

        int[][] myCoolNumbers2 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0}
        };
        System.out.println("Kolmanda rea viimane element: " +
                myCoolNumbers[2][myCoolNumbers2[2].length - 1]);

        System.out.println("Ex 10");
        // Variant 1: Inline
        String[][] countryCities = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko", "Jämsä"}
        };

        // Variant 2: Elements one-by-one
        String[][] countryCities2 = new String[3][4];
        countryCities2[0][0] = "Tallinn";
        countryCities2[0][1] = "Tartu";
        countryCities2[0][2] = "Valga";
        countryCities2[0][3] = "Võru";

        countryCities2[1][0] = "Stockholm";
        countryCities2[1][1] = "Uppsala";
        countryCities2[1][2] = "Lund";
        countryCities2[1][3] = "Köping";

        countryCities2[2][0] = "Helsinki";
        countryCities2[2][1] = "Espoo";
        countryCities2[2][2] = "Hanko";
        countryCities2[2][3] = "Jämsä";

        System.out.println("Teise riigi kolmas linn: " + countryCities2[1][2]);
        System.out.println("Esimese riigi esimene linn: " + countryCities2[0][0]);
    }
}
