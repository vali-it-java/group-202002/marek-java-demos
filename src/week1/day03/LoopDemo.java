package week1.day03;

public class LoopDemo {
    public static void main(String[] args) {

        // -------------------
        // for Loop
        // -------------------
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris", "Oslo"};

        System.out.println("Linnad õigetpidi:");
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        System.out.println("Linnad tagurpidi:");
        for (int i = cities.length - 1; i >= 0; i--) {
            System.out.println(cities[i]);
        }

        System.out.println("Break keywordi kasutamine:");
        for (int i = 0; i < cities.length; i++) {
            if (cities[i].equals("Madrid")) {
                System.out.println("Fataalne situatsioon. Lõpetame tsükli tätmise...");
                break; // Tuvastatud tingimus, mis nõuab tsükli täitmise viivitamatut poolelijätmist.
            } else {
                System.out.println(cities[i]);
            }
        }

        System.out.println("Continue keywordi kasutamine:");
        for (int i = 0; i < cities.length; i++) {
            if (cities[i].equals("Madrid")) {
                System.out.println("Madridi peab ignoreerima. Alustame viivitamatult uut tsüklikorda järgmise elemendiga...");
                continue; // Tuvastatud tingimus, mis nõuab tsükli täitmise viivitamatut poolelijätmist.
            } else {
                System.out.println(cities[i]);
            }
        }

        String[][] countryCities = {
                {"Tallinn", "Tartu", "Valga", "Võru", "Narva"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko"}
        };

        // Kahetasandilise massiivi läbiloopimine
        for (int k = 0; k < countryCities.length; k++) { // read
            System.out.println("Rida indeksiga " + k);
            for (int j = 0; j < countryCities[k].length; j++) { // veerud
                System.out.println(countryCities[k][j]);
            }
        }

        // -------------------
        // for-each Loop
        // -------------------
        String[] cities2 = {"Tallinn", "Helsinki", "Madrid", "Paris", "Oslo"};

        System.out.println("For-each loop...");
        for (String myCity : cities2) {
            System.out.println(myCity);
        }

        String[][] countryCities2 = {
                {"Tallinn", "Tartu", "Valga", "Võru", "Narva"},
                {"Stockholm", "Uppsala", "Lund", "Köping"},
                {"Helsinki", "Espoo", "Hanko"}
        };

        System.out.println("Kahetasandiline massiiv for-each meetodil...");
        for (String[] myCountryCities : countryCities2) {
            for (String city : myCountryCities) {
                System.out.println(city);
            }
        }

        // -------------------
        // while Loop
        // -------------------

        System.out.println("while loop...");
        String[] cities3 = {"Tallinn", "Helsinki", "Madrid", "Paris", "Oslo"};
        int myCityIndex = 0;
        while (myCityIndex < cities3.length) {
            System.out.println(cities3[myCityIndex]);
            myCityIndex++;
        }

        // -------------------
        // do-while Loop
        // -------------------

        System.out.println("do-while loop...");
        String[] cities4 = {"Tallinn", "Helsinki", "Madrid", "Paris", "Oslo"};

        int i = 0;
        do {
            System.out.println(cities4[i]);
            i++;
        } while (i < cities4.length);

    }
}
