package week1.day03;

public class ArraysDemo {

    public static void main(String[] args) {
        String[] weekendDays = {"Laupäev", "Pühapäev"};

        String[][] persons = {
                {"Mati", "Kask", "Võnnu"},
                {"Malle", "Tamm", "Kärdla"}
        };

        System.out.println("Viimane inimene elab kohas nimega: " + persons[1][2]);
        persons[1][2] = "Tartu";
        System.out.println("Viimane inimene elab kohas nimega: " + persons[1][2]);

        String[][] persons2 = new String[2][3];
        persons2[0][0] = "Mati";
        persons2[0][1] = "Kask";
        persons2[0][2] = "Võnnu";
        persons2[1][0] = "Malle";
        persons2[1][1] = "Tamm";
        persons2[1][2] = "Kärdla";

    }

}
