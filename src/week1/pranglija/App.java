package week1.pranglija;

import java.util.Scanner;

public class App {
    private static final int GUESS_COUNT = 3;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        while (true) {
            boolean hasFailed = false;
            System.out.println("--------------------------------");
            System.out.println("Mäng algas!");
            // Küsime kasuatajalt, mis vahemikus ta soovib mängida...
            System.out.print("Sisesta numbrivahemiku miinimum: ");
            int lower = getNumberFromConsole();

            System.out.print("Sisesta numbrivahemiku maksimum: ");
            int upper = 0;
            while(upper <= lower) {
                upper = getNumberFromConsole();
                System.out.println("Numbrivahemiku maksimum peab olema suurem, kui miinimum!");
            }

            long gameStartTime = System.currentTimeMillis() / 1000;

            System.out.println("Arva õigesti " + GUESS_COUNT + " korda!");
            for (int i = 1; i <= GUESS_COUNT; i++) {
                int number1 = (int) (Math.random() * (upper + 1 - lower)) + lower;
                int number2 = (int) (Math.random() * (upper + 1 - lower)) + lower;

                System.out.print("Ütle summa: " + number1 + " + " + number2 + " = ");
                int userInput = getNumberFromConsole();
                if (userInput == number1 + number2) {
                    System.out.println("Tubli, sa arvasid õigesti!");
                } else {
                    hasFailed = true;
                    break;
                }
            }

            if (hasFailed == false) {
                long gameEndTime = System.currentTimeMillis() / 1000;
                long duration = gameEndTime - gameStartTime;
                System.out.println("Sa võitsid! Sul kulus " + duration + " sekundit.");
            } else {
                System.out.println("Sa arvasid valesti. Mäng läbi, sa kaotasid!");
            }

            // Kas jätkata mängu...
            while (true) {
                System.out.println("Kas soovid jätkata uue mänguga (0 - ei, 1 - jah)?");
                int userDecision = getNumberFromConsole();
                if (userDecision == 0) {
                    return;
                } else if (userDecision == 1) {
                    break;
                } else {
                    System.out.println("Ei saa aru. Mida sa mõtlesid?");
                }
            }
        }

    }

    private static int getNumberFromConsole() {
        int numberFromConsole;
        while (true) {
            try {
                numberFromConsole = Integer.parseInt(scanner.nextLine());
                break;
            } catch (NumberFormatException e) {
                System.out.println("Sisestatud tekst ei ole number. Proovi uuesti.");
            }
        }
        return numberFromConsole;
    }
}
