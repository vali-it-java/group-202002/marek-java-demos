package week2.day09;

import java.util.Objects;

public class Worker implements Comparable<Worker> {
    private String name;
    private String profession;
    private int workedYears;
    private int salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public int getWorkedYears() {
        return workedYears;
    }

    public void setWorkedYears(int workedYears) {
        this.workedYears = workedYears;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Worker(String name, String profession, int workedYears, int salary) {
        this.name = name;
        this.profession = profession;
        this.workedYears = workedYears;
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", profession='" + profession + '\'' +
                ", workedYears=" + workedYears +
                ", salary=" + salary +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return workedYears == worker.workedYears &&
                salary == worker.salary &&
                Objects.equals(name, worker.name) &&
                Objects.equals(profession, worker.profession);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, profession, workedYears, salary);
    }

    @Override
    public int compareTo(Worker worker) {
        /*
            Kui see objekt on võrdne worker objektiga ==> tagasta 0
            Kui see objekt on suurem, kui worker objekt ==> tagasta positiivne number (1 ... 2.47 miljardit)
            Kui see objekt on väiksem, kui worker objekt ==> tatast negatiivne number (-1 ... -2.47 miljardit)
        */

        // Sorteerimine nime järgi.
//        return this.getName().compareTo(worker.getName());

        // Sorteerimine palga järgi kasvavalt.
//        if (this.getSalary() > worker.getSalary()) { // Kas selle töötaja palk on suurem, kui teise oma?
//            return 1;
//        } else if (this.getSalary() < worker.getSalary()) { // Kas selle töötaja palk on väiksem, kui teise oma?
//            return -1;
//        } else {
//            // Järelikult nende töötajate palgad on võrdsed.
//            return 0;
//        }
        return this.getSalary() - worker.getSalary();
    }
}
