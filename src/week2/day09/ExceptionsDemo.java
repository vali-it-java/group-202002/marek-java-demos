package week2.day09;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;

public class ExceptionsDemo {
    public static void main(String[] args) {
        try {
            List<String> fileLines = Files.readAllLines(Paths.get("resources/kontod.txt"));
            int[] array = new int[3];
            System.out.println(array[8]);
        }
        catch (NoSuchFileException e) {
            System.out.println("Ilmnes viga. Ei suutnud lugeda või avada faili.");
            System.out.println("Viga: " + e.toString());
        }
        catch (IOException e) {
            System.out.println("Ilmnes mingi üldine failisüsteemi viga.");
            System.out.println("Viga: " + e.toString());
        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Sa üritad küsida massiivi elementi, mida pole olemas.");
            System.out.println("Viga: " + e.toString());
        }
        catch (Exception e) {
            System.out.println("Ilmnes mingi täiesti ootamatu jama. Ei kujuta ette, mis juhtus.");
            System.out.println("Viga: " + e.toString());
        }
    }
}
