package week2.day09;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparingObjects {
    public static void main(String[] args) {

        Worker worker1 = new Worker("Mati", "driver", 5, 1500);
        Worker worker2 = new Worker("Mati", "driver", 5, 1500);
        Worker worker3 = new Worker("Kati", "accountant", 15, 1900);
        Worker worker4 = new Worker("Albert", "pilot", 8, 2200);

        System.out.println("Kas Matid on võrdsed? " + worker1.equals(worker2));
        System.out.println("Mati 1 räsikood: " + worker1.hashCode());
        System.out.println("Mati 2 räsikood: " + worker2.hashCode());

        System.out.println("Sorteerima töötajad.");
        System.out.println("Küsimus: mille alusel?");

        List<Worker> myWorkers = new ArrayList<>();
        myWorkers.add(worker1);
        myWorkers.add(worker2);
        myWorkers.add(worker3);
        myWorkers.add(worker4);

        System.out.println("Enne sorteerimist:");
        System.out.println(myWorkers);
        Collections.sort(myWorkers);
        System.out.println("Pärast sorteerimist (\"native\" sort):");
        System.out.println(myWorkers);
        System.out.println("Sorteerimine välise sorteerimisinstruktsiooni abil:");
        // Variant 1 (kompaktne lambda avaldis)
//        Collections.sort(myWorkers, (w1, w2) -> w1.getWorkedYears() - w2.getWorkedYears());

        // Variant 2 (pikemalt välja kirjutatud lambda avaldis - vajalik siis,
        // kui statemente on rohkem kui üks)
//        Collections.sort(myWorkers, (w1, w2) -> {
//            // ütleme siin, kas w1 on väiksem kui w2.
//
//            // Ülesanne, sorteerime töötajad töötatud aastate järgi kavavalt.
//            return w1.getWorkedYears() - w2.getWorkedYears();
//        });

        // Variant 3: funktsiooni pointeri kaudu sorteerimine
//        Collections.sort(myWorkers, Comparator.comparingInt(Worker::getWorkedYears));

        // Variant 4 (kompaktne lambda avaldis)
//        Collections.sort(myWorkers, (w1, w2) -> w1.getWorkedYears() - w2.getWorkedYears());
        Collections.sort(myWorkers, new WorkerComparator());

        System.out.println(myWorkers);
    }

    public static int calculateRectangleArea(int a, int b) {
        return a * b;
    }
}
