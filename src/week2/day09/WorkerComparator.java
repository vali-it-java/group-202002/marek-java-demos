package week2.day09;

import java.util.Comparator;

public class WorkerComparator implements Comparator<Worker> {
    @Override
    public int compare(Worker w1, Worker w2) {
        return w1.getWorkedYears() - w2.getWorkedYears();
    }
}
