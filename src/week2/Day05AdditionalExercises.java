package week2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day05AdditionalExercises {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        for (int k = 1; k <= 8; k++) {
            for (int l = 1; l <= 19; l++) {
                if (k % 2 == 1) {
                    System.out.print(l % 2 == 1 ? "#" : "+");
                } else {
                    System.out.print(l % 2 == 1 ? "+" : "#");
                }
            }
            System.out.println();
        }

        System.out.println("Ex 2");
        String personsTxt = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA";

        // Step 1: tekitage 5-elemendiline ühetasandiline massiiv.
        // [
        //  "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti",
        //  "Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome",
        //  ...
        // ]
        String[] persons1 = personsTxt.split("; ");

        // Step 2: Lõhkuge iga isiku andmed massiiviks
        // "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti" ===>
        // ["Eesnimi: Teet", "Perenimi: Kask", "Vanus: 34", "Amet: lendur", "Kodakondsus: Eesti"]

        String[][] persons2 = new String[persons1.length][5];

        int index = 0;
        for (String person : persons1) {
            String[] personDetails = person.split(", ");

            // Must maagia
            // "Eesnimi: Teet" ==> "Teet"
            for (int i = 0; i < personDetails.length; i++) {
                String[] detailParts = personDetails[i].split(": ");
                personDetails[i] = detailParts[1];
            }

            persons2[index] = personDetails;
            index++;
        }

        /*
            Isik Teet Kask: vanus: 34, amet: lendur, kodakondsus: Eesti,
            Isik ...
        */
        System.out.println("Persons array:");
        for(String[] personDetails : persons2) {
            System.out.printf("Isik %s %s: vanus %s, amet: %s, kodakondsus: %s\n",
                    personDetails[0], personDetails[1], personDetails[2], personDetails[3], personDetails[4]);
        }

//        List<String[]> persons3 = Arrays.asList(personsTxt.split("; "))
//                .stream().map(personStr -> personStr.split(", "))
//                .collect(Collectors.toList());

        /*
            Eesmärk teha selline map:
             Map[
        	    "Teet Kask" --> {"34", "lendur", "Eesti"},
	            "Mari Tamm" --> {"56, "kosmonaut", "Soome"}
            ]
         */

        Map<String, List<String>> personsMap = new HashMap<>();

        for (String personDetailsTxt : personsTxt.split("; ")) {
            // personDetailsTxt =
            //  "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti";
            String[] personDetails = personDetailsTxt.split(", ");
            // personDetails =
            //  { "Eesnimi: Teet", "Perenimi: Kask", "Vanus: 34", "Amet: lendur", ... }
            String personKey = personDetails[0].split(": ")[1] + " " +
                    personDetails[1].split(": ")[1];
            // personKey = "Teet Kask"
            List<String> personsValue = Arrays.asList(
                    personDetails[2].split(": ")[1],
                    personDetails[3].split(": ")[1],
                    personDetails[4].split(": ")[1]
            );
            // personsValue = { "34", "lendur", "Eesti" }
            personsMap.put(personKey, personsValue);
        }

        System.out.println("Persons map:");
        System.out.println(personsMap);
    }
}
