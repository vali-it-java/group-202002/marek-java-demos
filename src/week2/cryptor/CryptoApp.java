package week2.cryptor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class CryptoApp {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        // Loeme sisse alfabeet faili rida haaval.
        List<String> fileLines = Files.readAllLines(Paths.get("resources/alfabeet.txt"));

        Encryptor encryptor = new Encryptor(fileLines);
        Decryptor decryptor = new Decryptor(fileLines);

        System.out.println("-----------------------");
        System.out.println("Vali IT spioonirakendus");
        System.out.println("-----------------------");
        System.out.println("Mida soovid teha?");
        System.out.println("Krüpteerimine: ENCRYPT salajane tekst");
        System.out.println("Dekrüpteerimine: DECRYPT krüpteeritud tekst");
        System.out.println("Väljumine: EXIT");
        System.out.println("-----------------------");
        while (true) {

            String input = scanner.nextLine().toUpperCase();

            if (input.startsWith("EXIT")) {
                System.out.println("Head aega!");
                return;
            } else if (input.startsWith("ENCRYPT")) {
                System.out.println("Krüpteeritud tekst:");
                System.out.println(encryptor.convert(input.substring(7).trim()));
            } else if (input.startsWith("DECRYPT")) {
                System.out.println("Dekrüpteeritud tekst:");
                System.out.println(decryptor.convert(input.substring(7).trim()));
            } else {
                System.out.println("Ei saa aru!");
            }
        }

        // ENCRYPT Võti on mati all
        // Krüpteeritud tekst: FDHŽ.PQ.RÜHŽ.ÜSS

        // DECRYPT FDHŽ.PQ.RÜHŽ.ÜSS
        // Dekrüpteeritud tekst: VÕTI ON MATI ALL

        // EXIT
        // [programm lõpetab töö]

    }
}
