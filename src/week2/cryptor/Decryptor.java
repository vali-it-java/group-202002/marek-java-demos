package week2.cryptor;

import java.util.List;

public class Decryptor extends Cryptor {

    public Decryptor(List<String> fileLines) {
        for (String line : fileLines) {
            String[] lineParts = line.split(", ");
            this.alphabet.put(lineParts[1], lineParts[0]);
        }
    }
}
