package week2.cryptor;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CryptorTest {

    private static List<String> fileLines;

    @BeforeAll
    public static void setupTestData() throws IOException {
        fileLines = Files.readAllLines(Paths.get("resources/alfabeet.txt"));
    }

    @Test
    public void testEncrypt() {
        String text = "Võti on mati all";
        String expectedResult = "FDHŽ.PQ.RÜHŽ.ÜSS";

        Encryptor encryptor = new Encryptor(fileLines);
        String actualResult = encryptor.convert(text);
        Assertions.assertTrue(actualResult.equals(expectedResult), "Text encrypted incorrectly.");
    }

    @Test
    public void testDecrypt() {
        String text = "FDHŽ.PQ.RÜHŽ.ÜSS";
        String expectedResult = "VÕTI ON MATI ALL";

        Decryptor decryptor = new Decryptor(fileLines);
        String actualResult = decryptor.convert(text);
        Assertions.assertEquals(expectedResult, actualResult, "Text decrypted incorrectly.");
    }

}
