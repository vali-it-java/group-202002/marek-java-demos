package week2.cryptor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) throws IOException {
        // Loeme sisse alfabeet faili rida haaval.
        List<String> fileLines = Files.readAllLines(Paths.get("resources/alfabeet.txt"));

        String text = "Võti on mati all";
        System.out.println("Algne tekst:");
        System.out.println(text);

        Encryptor encryptor = new Encryptor(fileLines);
        String encryptedText = encryptor.convert("Võti on mati all"); // Tulemus: "FDHŽ.PQ.RÜHŽ.ÜSS"

        // Krüpteeritud teksti väljaprintimine...
        System.out.println("Krüpteeritud tekst:");
        System.out.println(encryptedText);

        Decryptor decryptor = new Decryptor(fileLines);
        String decryptedText = decryptor.convert("FDHŽ.PQ.RÜHŽ.ÜSS"); // Tulemus: "VÕTI ON MATI ALL"

        // Dekrüpteeritud teksti väljaprintimine...
        System.out.println("Dekrüpteeritud tekst:");
        System.out.println(decryptedText);

//        // Tekitame alfabeet Mapi.
//        Map<String, String> alphabet = new HashMap<>();
//        for (String line : fileLines) {
//            // "D, Õ"
//
//            // Variant 1 (split)
//            String[] lineParts = line.split(", ");
//            alphabet.put(lineParts[0], lineParts[1]);
//
//            // variant 2 (substring)
////            String key = line.substring(0, 1);
////            String value = line.substring(3, 4);
////            alphabet.put(key, value);
//        }
//
//        String text = "Võti on mati all";
//        System.out.println("Algne tekst:");
//        System.out.println(text);
//
//        String encryptedText = "";
//
//        // Must maagia
//        for (char c : text.toCharArray()) {
//            String cStr = String.valueOf(c);
//            cStr = cStr.toUpperCase();
//            encryptedText = encryptedText + alphabet.get(cStr);
//        }
//
//        // Krüpteeritud teksti väljaprintimine...
//        System.out.println("Krüpteeritud tekst:");
//        System.out.println(encryptedText);
    }
}
