package week2.cryptor;

import java.util.List;

public class Encryptor extends Cryptor {

    public Encryptor(List<String> fileLines) {
        for (String line : fileLines) {
            String[] lineParts = line.split(", ");
            this.alphabet.put(lineParts[0], lineParts[1]);
        }
    }

}
