package week2.cryptor;

import java.util.HashMap;
import java.util.Map;

public abstract class Cryptor {
    protected Map<String, String> alphabet = new HashMap<>();

    public String convert(String text) {
        String resultText = "";
        for (char c : text.toCharArray()) {
            String cStr = String.valueOf(c);
            cStr = cStr.toUpperCase();
            resultText = resultText + this.alphabet.get(cStr);
        }
        return resultText;
    }
}
