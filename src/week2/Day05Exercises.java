package week2;

import java.lang.reflect.Array;
import java.util.*;

public class Day05Exercises {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        // Variant 1
//        for(int i = 1; i <= 6; i++) { // read
//            int colCount = 7 - i;
//            for(int j = 1; j <= colCount; j++) { // veerud
//                System.out.print("#");
//            }
//            System.out.println();
//        }

        // Variant 2
//        for(int i = 1; i <= 6; i++) { // read
//            int colCount = 7 - i;
//            System.out.println("#".repeat(colCount));
//        }

        // Variant 3
        for (int i = 6; i >= 1; i--) { // read
            System.out.println("#".repeat(i));
        }

        System.out.println("Ex 2");
        // Variant 1
//        for (int i = 1; i <= 6; i++) { // read
//            for (int j = 1; j <= 6; j++) { // veerud
//                if (j <= i) {
//                    System.out.print("#");
//                }
//            }
//            System.out.println();
//        }

        // Variant 2
        for (int i = 1; i <= 6; i++) {
            System.out.println("#".repeat(i));
        }

        System.out.println("Ex 3");
        // Variant 1
//        for (int i = 1; i <= 5; i++) { // read
//            for (int j = 1; j <= 5 - i; j++) { // veerud (vasakpoolne kolmnurk)
//                System.out.print(" ");
//            }
//            for (int k = 1; k <= i; k++) { // veerud (parempoolne kolmnurk)
//                System.out.print("@");
//            }
//            System.out.println();
//        }

        // Variant 2
        for (int i = 1; i <= 5; i++) {
            System.out.print(" ".repeat(5 - i));
            System.out.println("@".repeat(i));
        }

        System.out.println("Ex 4");
        // Variant 1
        int myNumber = 1234567;
        String myNumberTxt = String.valueOf(myNumber);
        char[] myNumberChars = myNumberTxt.toCharArray();
        String myReversedNumberTxt = "";

        for (int i = 0; i < myNumberChars.length; i++) {
            myReversedNumberTxt = myNumberChars[i] + myReversedNumberTxt;
        }

        int myReversedNumber = Integer.parseInt(myReversedNumberTxt);
        System.out.println("Minu ümberpööratud number: " + myReversedNumber);

        // Variant 2
        int myNumber2 = 389;
        StringBuilder sb = new StringBuilder(String.valueOf(myNumber2));
        String myReversedNumberTxt2 = sb.reverse().toString();
        int myReversedNumber2 = Integer.parseInt(myReversedNumberTxt2);
        System.out.println("Minu ümberpööratud number: " + myReversedNumber2);

        System.out.println("Ex 5");
        // Kui õpilasi on palju...
        // indeksid 0, 1 - õpilane 1
        // indeksid 2, 3 - õpilant 2
        // indeksid 4, 5 - õpilane 3
        for (int i = 0; i < args.length; i += 2) {
            String studentName = args[i];
            int points = Integer.parseInt(args[i + 1]);
            int grade = 0;

            // Leiame hinde
            if (points > 90) {
                grade = 5;
            } else if (points > 80) {
                grade = 4;
            } else if (points > 70) {
                grade = 3;
            } else if (points > 60) {
                grade = 2;
            } else if (points > 50) {
                grade = 1;
            }

            if (grade > 0) {
                System.out.printf("%s: PASS - %d, %d\n", studentName, grade, points);
            } else {
                System.out.printf("%s: FAIL\n", studentName);
            }
        }

        System.out.println("Ex 6");
        double[][] triangles = {
                {6.7, 4.8},
                {5.5, 2.9},
                {56.897, 89.993},
                {4, 3}
        };

        for (double[] triangle : triangles) {
            double sideA = triangle[0];
            double sideB = triangle[1];
            double sideC = Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
            System.out.printf("Kolmnurk külgedega %.2f ja %.2f: hüpotenuusi pikkus on %.2f\n",
                    sideA, sideB, sideC);
        }

        System.out.println("Ex 7");
        String[][] countries = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Finland", "Helsinki", "Sanna Marin"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"Russia", "Moscow", "Mikhail Mishustin"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"France", "Paris", "Édouard Philippe"}
        };

        System.out.println("Prime ministers only:");
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i][2]);
        }

        System.out.println("Country info:");
        for (int i = 0; i < countries.length; i++) {
            System.out.println("Country: " + countries[i][0] +
                    ", Capital: " + countries[i][1] + ", Prime minister: " + countries[i][2]);
        }

        System.out.println("Ex 8");
        String[][][] countries2 = {
                {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}},
                {{"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian"}},
                {{"Lithuania"}, {"Vilnius"}, {"Saulius Skvernelis"}, {}},
                {{"Finland"}, {"Helsinki"}, {"Sanna Marin"}, {"Finnish", "Estonian"}},
                {{"Sweden"}, {"Stockholm"}, {"Stefan Löfven"}, {"Swedish", "Finnish", "English"}},
                {{"Norway"}, {"Oslo"}, {"Erna Solberg"}, {"Norwegian", "Swedish", "Finnish", "English"}},
                {{"Denmark"}, {"Copenhagen"}, {"Mette Frederiksen"}, {"Danish", "English"}},
                {{"Russia"}, {"Moscow"}, {"Mikhail Mishustin"}, {"Russian", "English"}},
                {{"Germany"}, {"Berlin"}, {"Angela Merkel"}, {"German"}},
                {{"France"}, {"Paris"}, {"Édouard Philippe"}, {"French", "English"}}
        };

        // Variant 1
//        for(int i = 0; i < countries2.length; i++) {
//            System.out.println(countries2[i][0][0] + " " + countries2[i][2][0]);
//            for(int j = 0; j < countries2[i][3].length; j++) {
//                System.out.println("\t" + countries2[i][3][j]);
//            }
//        }

        // Variant 2
        for (String[][] country : countries2) {
            System.out.println(country[0][0] + ", " + country[1][0]);
            for (String language : country[3]) {
                System.out.println("\t" + language);
            }
        }

        System.out.println("Ex 9");
        List<List<List<String>>> countries3 = Arrays.asList(
                Arrays.asList(
                        Arrays.asList("Estonia"),
                        Arrays.asList("Tallinn"),
                        Arrays.asList("Jüri Ratas"),
                        Arrays.asList("Estonian", "Russian", "Finnish")
                ),
                Arrays.asList(
                        Arrays.asList("Latvia"),
                        Arrays.asList("Riga"),
                        Arrays.asList("Arturs Krišjānis Kariņš"),
                        Arrays.asList("Latvian", "Russian")
                ),
                Arrays.asList(
                        Arrays.asList("Lithuania"),
                        Arrays.asList("Vilnius"),
                        Arrays.asList("Saulius Skvernelis"),
                        Arrays.asList()
                )
        );

        // Variant 1
//        for (int i = 0; i < countries3.size(); i++) {
//            System.out.println(countries3.get(i).get(0).get(0) + ", " +
//                    countries3.get(i).get(2).get(0));
//            for (int j = 0; j < countries3.get(i).get(3).size(); j++) {
//                System.out.println("\t" + countries3.get(i).get(3).get(j));
//            }
//        }

        // Variant 2
        for (List<List<String>> country : countries3) {
            System.out.println(country.get(0).get(0) + ", " + country.get(2).get(0));
            for (String language : country.get(3)) {
                System.out.println("\t" + language);
            }
        }

        System.out.println("Ex 10");
        // Estonia --> [Tallinn], [Jüri Ratas], [Estonian, Russian, Finnish]
        Map<String, String[][]> countries4 = new HashMap<>();
        countries4.put("Estonia",
                new String[][]{{"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}});
        countries4.put("Latvia",
                new String[][]{{"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian"}});
        countries4.put("Denmark",
                new String[][]{{"Copenhagen"}, {"Mette Frederiksen"}, {"Danish", "English"}});

        for (String countryName : countries4.keySet()) {
            System.out.println(countryName + ", " + countries4.get(countryName)[1][0]);
            for (String language : countries4.get(countryName)[2]) {
                System.out.println("\t" + language);
            }
        }

        System.out.println("Ex 11");
        Queue<String[][]> countries5 = new LinkedList<>();
        countries5.add(new String[][]{
                {"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}});
        countries5.add(new String[][]{
                {"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian"}});
        countries5.add(new String[][]{
                {"Lithuania"}, {"Vilnius"}, {"Saulius Skvernelis"}, {}});

        while (!countries5.isEmpty()) {
            String[][] country = countries5.remove();
            System.out.println(country[0][0] + ", " + country[2][0]);
            for(String language : country[3]) {
                System.out.println("\t" + language);
            }
        }
    }
}
