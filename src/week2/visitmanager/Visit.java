package week2.visitmanager;

public class Visit {
    private String date;
    private int visitCount;

    public String getDate() {
        return date;
    }

    public int getVisitCount() {
        return visitCount;
    }

    public Visit(String date, int visitCount) {
        this.date = date;
        this.visitCount = visitCount;
    }
}
