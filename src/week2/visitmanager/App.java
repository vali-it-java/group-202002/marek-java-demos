package week2.visitmanager;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class App {
    private static List<Visit> visits = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        List<String> visitLines = Files.readAllLines(Paths.get("resources/visits.txt"));

        for (String line : visitLines) {
            String[] lineParts = line.split(", ");
            String date = lineParts[0];
            int visitCount = Integer.parseInt(lineParts[0]);
            Visit v = new Visit(date, visitCount);
            visits.add(v);
        }

        // Sorteerimiseks kaks varianti:
        // 1) Visit implements Comparable<Visit>
        // 2) Väline sorteerimisinstruktsioon
        //      (?, ?) -> ???
        
    }
}
