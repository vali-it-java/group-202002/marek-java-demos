package week2.day06;

public class MethodExercises {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        System.out.println("Meetod tagastas: " + test(3));

        System.out.println("Ex 2");
        test2("tere", null);

        System.out.println("Ex 3");
        System.out.println("Toote omahind: " + 15 + " eurot.");
        System.out.println("Toote letihind: " + addVat(15) + " eurot.");

        System.out.println("Ex 4");
        int[] myNumbers = anotherPointlessMethod(3, 5, true);
        System.out.println("Massiivi pikkus: " + myNumbers.length);

        System.out.println("Ex 5");
        printHello();

        System.out.println("Ex 6");
        System.out.println("Kas mees või naine? " + deriveGender("380.."));
        System.out.println("Kas mees või naine? " + deriveGender("280.."));

        System.out.println("Ex 7");
        System.out.println("Inimene isikukodiga 3810... on sündinud aastal "
                + deriveBirthYear("3810..."));
        System.out.println("Inimene isikukodiga 4810... on sündinud aastal "
                + deriveBirthYear("4810..."));
        System.out.println("Inimene isikukodiga 1270... on sündinud aastal "
                + deriveBirthYear("1270..."));

        System.out.println("Ex 8");
        System.out.println("Kas isikukood korrektne? " + validatePersonalCode("49403136515"));

        printNumbersDesc(100);
    }

    private static boolean test(int number) {
        return true;
    }

    public static void test2(String param1, String param2) {
        // NB: konsoolile printimine EI OLE tagastamine!
        System.out.println("Mina olen meetod, mis ei tee suurt midagi.");
    }

    private static double addVat(double productPrice) {
        return 1.2 * productPrice;
    }

    private static int[] anotherPointlessMethod(int num1, int num2, boolean isValid) {
        if (isValid) {
            return new int[]{num1, num2};
        } else {
            return new int[0];
        }
    }

    // See meetod ei tagasta midagi, ei võta vastu ühtki sisendparameetrit.
    // PANE TÄHELE: println() EI OLE tagastamine!
    private static void printHello() {
        System.out.println("Tere");
    }

    private static String deriveGender(String personalCode) {
        char initialDigitChar = personalCode.charAt(0);
        String initialDigitStr = String.valueOf(initialDigitChar);
        int initialDigit = Integer.parseInt(initialDigitStr);
        return initialDigit % 2 == 0 ? "F" : "M";
    }

    public static int deriveBirthYear(String personalCode) {
        int initialDigit = personalCode.charAt(0) - '0'; // 3
        int birthYearDigits = Integer.parseInt(personalCode.substring(1, 3)); // 81

        if (initialDigit == 1 || initialDigit == 2) { // 19. sajand
            return 1800 + birthYearDigits;
        } else if (initialDigit == 3 || initialDigit == 4) { // 20. sajand
            return 1900 + birthYearDigits;
        } else if (initialDigit == 5 || initialDigit == 6) { // 21. sajand
            return 2000 + birthYearDigits;
        } else if (initialDigit == 7 || initialDigit == 8) { // 22. sajand
            return 2100 + birthYearDigits;
        } else { // mingi jama
            return -1;
        }
    }

    public static boolean validatePersonalCode(String personalCode) { // "49403136515"
        if (personalCode == null || personalCode.length() != 11) {
            return false;
        }

        // Samm 1: "49403136515" ==> {4, 9, 4, 0, 3, 1, 1, 6, 5, 1, 5}
        int[] personalCodeDigits = new int[11];
        for (int i = 0; i < personalCodeDigits.length; i++) {
            personalCodeDigits[i] = personalCode.charAt(i) - '0';
        }

        // Samm 2: esimese astme kaalude defineerimine
        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};

        // Samm 3: arvutame välja isikukoodi numbrite ja vastava kaalu korrutiste summa
        int sum = 0;

        for (int i = 0; i < 10; i++) {
            sum = sum + weights1[i] * personalCodeDigits[i];
        }

        // Samm 4: arvutame välja kontrollnumbri
        int calculatedCheckSum = sum % 11; // 5

        if (calculatedCheckSum != 10) { // Lihtne case (lisaarvutust pole vaja)
            return calculatedCheckSum == personalCodeDigits[10];
        } else { // Keerukam case (vajame lisaarvutamist)
            // Samm 5: kontrollnumbri arvutamine teise astme kaaludega...
            int[] weights2 = {3, 4, 5, 6, 7, 8, 9, 1, 2, 3};
            sum = 0;
            for (int i = 0; i < 10; i++) {
                sum = sum + weights2[i] * personalCodeDigits[i];
            }
            calculatedCheckSum = sum % 11;
            if (calculatedCheckSum == 10) {
                calculatedCheckSum = 0;
            }
            return calculatedCheckSum == personalCodeDigits[10];
        }
    }

    public static void printNumbersDesc(int number) {
        System.out.println(number);
        if (number > 1) {
            printNumbersDesc(--number);
        }
    }
}
