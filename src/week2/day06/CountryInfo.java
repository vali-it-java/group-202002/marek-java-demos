package week2.day06;

public class CountryInfo {
    private String name;
    private String capital;
    private String primeMinister;
    private String[] languages;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getPrimeMinister() {
        return primeMinister;
    }

    public void setPrimeMinister(String primeMinister) {
        this.primeMinister = primeMinister;
    }

    public String[] getLanguages() {
        return languages;
    }

    public void setLanguages(String[] languages) {
        this.languages = languages;
    }

    public CountryInfo() {
    }

    public CountryInfo(String name, String primeMinister) {
        this.name = name;
        this.primeMinister = primeMinister;
    }

    public CountryInfo(String name, String capital, String primeMinister, String[] languages) {
        this.name = name;
        this.capital = capital;
        this.primeMinister = primeMinister;
        this.languages = languages;
    }

    @Override
    public String toString() {
        String info = this.name + ", " + this.primeMinister + "\n";
        for (String lang : this.languages) {
            info = info + "\t" + lang + "\n";
        }
        return info;
    }
}
