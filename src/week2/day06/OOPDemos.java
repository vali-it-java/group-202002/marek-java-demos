package week2.day06;

import java.util.ArrayList;
import java.util.List;

public class OOPDemos {
    public static void main(String[] args) {

        Person person1 = new Person();
        person1.firstName = "Mari";
        person1.lastName = "Kask";
        person1.personalCode = "61107121760";
        person1.profession = "Accountant";
        person1.salary = 2000;

        Person person2 = new Person();
        person2.firstName = "Kalle";
        person2.lastName = "Kapp";
        person2.personalCode = "50309111760";
        person2.profession = "Driver";
        person2.salary = 1800;

        List<Person> myEmployees = new ArrayList<>();
        myEmployees.add(person1);
        myEmployees.add(person2);

        String firstPersonFirstName = myEmployees.get(0).firstName;
        String secondPersonFirstName = myEmployees.get(1).firstName;

        int firstPersonBirthYear = person1.getBirthYear();
        System.out.println("Esimese töötaja sünniaasta: " + firstPersonBirthYear);

        int secondPersonBirthYear = person2.getBirthYear();
        System.out.println("Teise töötaja sünniaasta: " + secondPersonBirthYear);

        String person1Gender = person1.getGender(); // "F"
        System.out.println("Esimese töötaja sugu: " + person1Gender);

        String person2Gender = person2.getGender(); // "M"
        System.out.println("Teise töötaja sugu: " + person2Gender);

        int person1BirthDay = person1.getBirthDayOfMonth();
        System.out.println("Esimese töötaja sünnikuupäev: " + person1BirthDay);

        int person2BirthDay = person2.getBirthDayOfMonth();
        System.out.println("Teise töötaja sünnikuupäev: " + person2BirthDay);

        String person1BirthMonth = person1.getBirthMonth();
        System.out.println("Esimese töötaja sünnikuu: " + person1BirthMonth);

        String person2BirthMonth = person2.getBirthMonth();
        System.out.println("Teise töötaja sünnikuu: " + person2BirthMonth);

        System.out.println("Esimese töötaja sünniaeg ilusti kirjeldatuna: " +
                person1.getBeautifulBirthDateText());

        System.out.println("Teise töötaja sünniaeg ilusti kirjeldatuna: " +
                person2.getBeautifulBirthDateText());
    }
}
