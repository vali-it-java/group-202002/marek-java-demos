package week2.day06;

public class OOPExercises {
    public static void main(String[] args) {

        System.out.println("Ex 1");
        // {"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}
//        CountryInfo estonia = new CountryInfo();
//        estonia.name = "Estonia";
//        estonia.capital = "Tallinn";
//        estonia.primeMinister = "Jüri Ratas";
//        estonia.languages = new String[]{"Estonian", "Russian", "Finnish"};

        CountryInfo estonia = new CountryInfo(
                "Estonia", "Tallinn", "Jüri Ratas",
                new String[]{"Estonian", "Russian", "Finnish"});

        CountryInfo latvia = new CountryInfo();
        latvia.setName("Latvia");
        latvia.setCapital("Riga");
        latvia.setPrimeMinister("Arturs Krišjānis Kariņš");
        latvia.setLanguages(new String[]{"Latvian", "Russian"});

        CountryInfo lithuania = new CountryInfo();
        lithuania.setName("Lithuania");
        lithuania.setCapital("Vilnius");
        lithuania.setPrimeMinister("Saulius Skvernelis");
        lithuania.setLanguages(new String[0]);

        CountryInfo[] countries = {
                estonia, latvia, lithuania
        };

        // for tsükkel
//        for (int i = 0; i < countries.length; i++) {
//            System.out.println(countries[i].name + ", " + countries[i].primeMinister);
//            for (int j = 0; j < countries[i].languages.length; j++) {
//                System.out.println("\t" + countries[i].languages[j]);
//            }
//        }

        // foreach tsükkel
//        for (CountryInfo country : countries) {
//            System.out.println(country.name + ", " + country.primeMinister);
//            for (String language : country.languages) {
//                System.out.println("\t" + language);
//            }
//        }

        // Riigi väljaprintimine objekti enda toString() meetodi abil.
        for (CountryInfo country : countries) {
            System.out.println(country);
        }

    }
}
