package week2.day06;

public class Person {

    // Objekt on koopia kõigi selle klassi mittestaatilistest liikmetest (members).

    // Objektil on seisund (state) - kõigi objektimuutujate kombinatsioon.
    public String firstName;
    public String lastName;
    public String profession;
    public String personalCode;
    public double salary;

    // Objektil on ka käitumine (behaviour) - kõik mittestaatilised meetodid,
    // mis teavad, mis objekti küljas nad on.
    public String describeYourself() {
        return "Employee: first name: " + this.firstName + ", last name: " + this.lastName;
    }

    public int getBirthYear() {
        int initialDigit = this.personalCode.charAt(0) - '0'; // 3
        int birthYearDigits = Integer.parseInt(this.personalCode.substring(1, 3)); // 81

        if (initialDigit == 1 || initialDigit == 2) { // 19. sajand
            return 1800 + birthYearDigits;
        } else if (initialDigit == 3 || initialDigit == 4) { // 20. sajand
            return 1900 + birthYearDigits;
        } else if (initialDigit == 5 || initialDigit == 6) { // 21. sajand
            return 2000 + birthYearDigits;
        } else if (initialDigit == 7 || initialDigit == 8) { // 22. sajand
            return 2100 + birthYearDigits;
        } else { // mingi jama
            return -1;
        }
    }

    public String getGender() {
        char initialDigitChar = this.personalCode.charAt(0);
        String initialDigitStr = String.valueOf(initialDigitChar);
        int initialDigit = Integer.parseInt(initialDigitStr);
        return initialDigit % 2 == 0 ? "F" : "M";
    }

    public int getBirthDayOfMonth() {
        return Integer.parseInt(this.personalCode.substring(5, 7));
    }

    public String getBirthMonth() {
        int birthMonth = Integer.parseInt(this.personalCode.substring(3, 5));

        switch (birthMonth) {
            case 1:
                return "jaanuar";
            case 2:
                return "veebruar";
            case 3:
                return "märts";
            case 4:
                return "aprill";
            case 5:
                return "mai";
            case 6:
                return "juuni";
            case 7:
                return "juuli";
            case 8:
                return "august";
            case 9:
                return "september";
            case 10:
                return "oktoober";
            case 11:
                return "november";
            case 12:
                return "detsember";
            default:
                return "VIGA! tundmatu kuu";
        }
    }

    public String getBeautifulBirthDateText() {
        return this.getBirthDayOfMonth() + ". " + this.getBirthMonth() + " " + this.getBirthYear();
    }

    // NON-OOP

    public static String describeYourself(Person person) {
        return "Employee: first name: " + person.firstName + ", last name: " + person.lastName;
    }
}
