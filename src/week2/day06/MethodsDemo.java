package week2.day06;

public class MethodsDemo {
    public static void main(String[] args) {
        int myNumber = 5;
        int myTripledNumber = tripleNumber(myNumber); // funktsiooni käivitamine
    }

    // Meetodi definitsioon
    // private - nähtav ainult sellest konkreetsest klassist
    // static - see meetod kuulub klassile, mitte selle klassi baasil loodud objektile
    // int - see meetod tagastab täisarvu
    // tripleNumber - meetodi nimi (võiks olla käskivas kõneviisis)
    // (int x) - sisendparameetrite plokk.
    // tripleNumber(int x) - see on meetodi signatuur
    private static int tripleNumber(int x) {
        return 3 * x;
    }

    // Meetodid tripleNumber(int x) ja tripleNumber(double x) on OVERLOAD'itud.
    private static double tripleNumber(double x) {
        return 3 * x;
    }

}
