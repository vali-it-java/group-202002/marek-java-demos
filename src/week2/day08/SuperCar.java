package week2.day08;

public class SuperCar extends Car {

    public void accelerateFast() {
        System.out.println("Accelerating very very fast...");
    }

    public SuperCar(String manufacturer, String model, int maxSpeed) {
        super(manufacturer, model, maxSpeed);
    }
}
