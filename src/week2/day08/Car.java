package week2.day08;

public class Car implements Movable {
    private String manufacturer;
    private String model;
    private int maxSpeed;
    private double currentSpeed;

    public Car(String manufacturer, String model, int maxSpeed) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public String getModel() {
        return model;
    }

    @Override
    public int getMaxSpeed() {
        return maxSpeed;
    }

    @Override
    public void speedUp(double targetSpeed) {
        this.currentSpeed = targetSpeed;
    }

    @Override
    public boolean slowDown(double amount) {
        this.currentSpeed = this.currentSpeed - amount;
        return this.currentSpeed <= 0;
    }

    @Override
    public String toString() {
        return "Car{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", currentSpeed=" + currentSpeed +
                '}';
    }
}
