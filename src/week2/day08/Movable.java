package week2.day08;

public interface Movable {
    String getManufacturer();
    String getModel();
    int getMaxSpeed();
    void speedUp(double targetSpeed);
    boolean slowDown(double amount);
}
