package week2.day08;

public class OOPExercises {
    public static void main(String[] args) {
        SuperCar ferrariF355 = new SuperCar("Ferrari", "F355", 310);
        Car opelCorsa = new Car("Opel", "Corsa", 145);
        Movable renaultMegane = new Car("Renault", "Megane", 175);

        ferrariF355.accelerateFast();

    }
}
