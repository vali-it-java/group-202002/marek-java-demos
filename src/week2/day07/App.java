package week2.day07;

public class App {

    public static void main(String[] args) {
        Athlete x = new Runner("Mati", "Veski", 45, "M", 180, 78);
        System.out.println("Jooksja...");
        System.out.println("Eesnimi: " + x.getFirstName());
        System.out.println("Perenimi: " + x.getLastName());
        x.perform();

        Skydiver y = new Skydiver("Anna", "Skywalker", 32, "F", 171, 62);
        System.out.println("Langevarjur...");
        System.out.println("Eesnimi: " + y.getFirstName());
        System.out.println("Perenimi: " + y.getLastName());
        y.freeFallTime();



    }
}
