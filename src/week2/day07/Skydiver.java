package week2.day07;

public class Skydiver extends Athlete {

    public Skydiver(String firstName, String lastName, int age, String gender, int height, double weight) {
        super(firstName, lastName, age, gender, height, weight);
    }

    public double freeFallTime() {
        return 56;
    }

    @Override
    public void perform() {
        System.out.println("Falling from sky...");
    }
}
