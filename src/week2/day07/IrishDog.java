package week2.day07;

public class IrishDog extends Dog {
    public IrishDog(String name) {
        super(name);
    }

    @Override
    public String bark() {
        String barkingSound = "amh-amh";
        System.out.println(barkingSound);
        return barkingSound;
    }
}
