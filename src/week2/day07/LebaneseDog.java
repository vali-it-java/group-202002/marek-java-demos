package week2.day07;

public class LebaneseDog extends Dog {
    public LebaneseDog(String name) {
        super(name);
    }

    @Override
    public String bark() {
        String barkingSound = "haw-haw";
        System.out.println(barkingSound);
        return barkingSound;
    }
}
