package week2.day07;

public class DogManager {
    public static void main(String[] args) {

        // Greek dog...
        GreekDog greekDog = new GreekDog("Poseidon");
        System.out.println("Greek dog barking...");
        System.out.println(greekDog);
        greekDog.bark();

        // Irish dog...
        Dog irishDog = new IrishDog("Aili");
        System.out.println("Irish dog barking...");
        System.out.println(irishDog);
        irishDog.bark();

        // Lebanese dog...
        Dog lebaneseDog = new LebaneseDog("Halim");
        System.out.println("Lebanese dog barking...");
        System.out.println(lebaneseDog);
        lebaneseDog.bark();

    }
}
