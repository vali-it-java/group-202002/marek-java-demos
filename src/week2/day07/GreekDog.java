package week2.day07;

public class GreekDog extends Dog {
    public GreekDog(String name) {
        super(name);
    }

    @Override
    public String bark() {
        String barkingSound = "ghav-ghav";
        System.out.println(barkingSound);
        return barkingSound;
    }
}
