package week2.day07;

public abstract class Dog {

    private String name;

    public String getName() {
        return name;
    }

    public Dog(String name) {
        this.name = name;
    }

    public abstract String bark();

    @Override
    public String toString() {
        return "Dog{" +
                "name='" + name + '\'' +
                ", barking sound: " + this.bark() +
                '}';
    }
}
