package week2.bank;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class AccountServiceTest {

    @BeforeAll
    public static void setUp() throws IOException {
        AccountService.loadAccounts("resources/kontod.txt");
    }

    @Test
    public void testFindAccountByNumber() {
        // Pete, Massey, 318129395, 6
        Account peteAccount = AccountService.findAccount("318129395");
        Assertions.assertNotNull(peteAccount);
        Assertions.assertEquals(6, peteAccount.getBalance());
        Assertions.assertEquals("Pete", peteAccount.getFirstName());
        Assertions.assertEquals("Massey", peteAccount.getLastName());
        Assertions.assertEquals("318129395", peteAccount.getAccountNumber());

        // Elijah, Munoz, 447912493, 28
        Account elijahAccount = AccountService.findAccount("447912493");
        Assertions.assertNotNull(elijahAccount);
        Assertions.assertEquals(28, elijahAccount.getBalance());
        Assertions.assertEquals("Elijah", elijahAccount.getFirstName());
        Assertions.assertEquals("Munoz", elijahAccount.getLastName());
        Assertions.assertEquals("447912493", elijahAccount.getAccountNumber());

    }

    @Test
    public void testFindAccountByName() {
        // Pete, Massey, 318129395, 6
        Account peteAccount = AccountService.findAccount("Pete", "Massey");
        Assertions.assertNotNull(peteAccount);
        Assertions.assertEquals(6, peteAccount.getBalance());
        Assertions.assertEquals("Pete", peteAccount.getFirstName());
        Assertions.assertEquals("Massey", peteAccount.getLastName());
        Assertions.assertEquals("318129395", peteAccount.getAccountNumber());

        // Elijah, Munoz, 447912493, 28
        Account elijahAccount = AccountService.findAccount("Elijah", "Munoz");
        Assertions.assertNotNull(elijahAccount);
        Assertions.assertEquals(28, elijahAccount.getBalance());
        Assertions.assertEquals("Elijah", elijahAccount.getFirstName());
        Assertions.assertEquals("Munoz", elijahAccount.getLastName());
        Assertions.assertEquals("447912493", elijahAccount.getAccountNumber());
    }

    @Test
    public void testTransfer() {
        // Payer: Daryl, Davis, 653797240, 2500
        // Beneficiary: Guadalupe, Newton, 427037539, 465
        Account payer = AccountService.findAccount("653797240");
        Account beneficiary = AccountService.findAccount("427037539");
        Assertions.assertEquals(2500, payer.getBalance());
        Assertions.assertEquals(465, beneficiary.getBalance());

        TransferResult result = AccountService
                .transfer("653797240", "427037539", 1_000_000);
        Assertions.assertFalse(result.isSuccess());

        result = AccountService
                .transfer("1111", "427037539", 10);
        Assertions.assertFalse(result.isSuccess());

        result = AccountService
                .transfer("653797240", "3333", 10);
        Assertions.assertFalse(result.isSuccess());

        result = AccountService
                .transfer("653797240", "427037539", 10);
        Assertions.assertTrue(result.isSuccess());
        Assertions.assertEquals(2490, result.getFromAccount().getBalance());
        Assertions.assertEquals(475, result.getToAccount().getBalance());

        result = AccountService
                .transfer("653797240", "427037539", -10);
        Assertions.assertTrue(result.isSuccess());
        Assertions.assertEquals(2480, result.getFromAccount().getBalance());
        Assertions.assertEquals(485, result.getToAccount().getBalance());
    }
}
