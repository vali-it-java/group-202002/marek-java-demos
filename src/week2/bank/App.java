package week2.bank;

import java.io.IOException;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws IOException {
        AccountService.loadAccounts("resources/kontod.txt");
        Scanner scanner = new Scanner(System.in);

        displayInstructions();
        while (true) {
            System.out.println("Sisesta käsklus:");
            String input = scanner.nextLine().toUpperCase();
            if (input.startsWith("EXIT")) {
                System.out.println("Head aega!");
                return;
            } else if (input.startsWith("BALANCE")) {
                String command = input.substring(7).trim();
                String[] commandParts = command.split(" ");
                if (commandParts.length == 1) {
                    // Konto detailandmete küsimine konto numbri järgi
                    Account account = AccountService.findAccount(commandParts[0]);
                    displayAccountDetails(account);
                } else if (commandParts.length > 1) {
                    // Konto detailandmete küsimine kontoomaniku ees- ja perenime järgi.
                    Account account = AccountService.findAccount(commandParts[0], commandParts[1]);
                    displayAccountDetails(account);
                } else {
                    System.out.println("Ei saa aru!");
                }

            } else if (input.startsWith("TRANSFER")) {
                String command = input.substring(8).trim();
                String[] commandParts = command.split(" ");
                if (commandParts.length == 3) {
                    TransferResult result = AccountService
                            .transfer(
                                    commandParts[0],
                                    commandParts[1],
                                    Integer.parseInt(commandParts[2]));
                    displayTransferResult(result);
                } else {
                    System.out.println("Ei saa aru!");
                }
            }
        }
    }

    private static void displayInstructions() {
        System.out.println("--------------------------");
        System.out.println("Vali IT pangarakendus");
        System.out.println("--------------------------");
        System.out.println("Kasutamine:");
        System.out.println("\tEXIT - lõpetab programmi töö");
        System.out.println("\tBALANCE <KONTO NUMBER> - väljastab konto detailandmed");
        System.out.println("\tBALANCE <EESNIMI> <PERENIMI> - väljastab konto detailandmed");
        System.out.println("\tTRANSFER <MAKSJA KONTO NUMBER> <SAAJA KONTO NUMBER> <SUMMA> - teostab ülekande");
    }

    private static void displayAccountDetails(Account account) {
        if (account != null) {
            // Kuvame konto detailandmed
            System.out.println("----------------------");
            System.out.println("Nimi: " + account.getFirstName() + " " + account.getLastName());
            System.out.println("Konto number: " + account.getAccountNumber());
            System.out.println("Konto jääk: " + account.getBalance() + " EUR");
            System.out.println("----------------------");
        } else {
            System.out.println("----------------------");
            System.out.println("Kontot ei leitud!");
            System.out.println("----------------------");
        }
    }

    private static void displayTransferResult(TransferResult result) {
        if (result.isSuccess()) {
            System.out.println("----------------------");
            System.out.println("Ülekande teostamine õnnestus!");
            System.out.println("Maksja konto:");
            displayAccountDetails(result.getFromAccount());
            System.out.println("Saaja konto:");
            displayAccountDetails(result.getToAccount());
        } else {
            System.out.println("----------------------");
            System.out.println("Ülekande teostamine ebaõnnestus!");
            System.out.println("Põhjus: " + result.getMessage());
            System.out.println("----------------------");
        }
    }
}
