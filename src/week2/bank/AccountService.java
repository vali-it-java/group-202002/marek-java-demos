package week2.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class AccountService {
    private static List<Account> accounts = new ArrayList<>();

    public static void loadAccounts(String filePath) throws IOException {
        // Loeme sisse faili
        List<String> accountLines = Files.readAllLines(Paths.get(filePath));

        for (String line : accountLines) {
            // "Thomas, Castillo, 249299846, 842" ==> new Account{Thomas, Castillo, 249299846, 842}
            String[] lineParts = line.split(", ");
            String firstName = lineParts[0];
            String lastName = lineParts[1];
            String accountNumber = lineParts[2];
            int balance = Integer.parseInt(lineParts[3]);
            Account x = new Account(firstName, lastName, accountNumber, balance);
            accounts.add(x);
        }
    }

    public static Account findAccount(String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;
    }

    public static Account findAccount(String firstName, String lastName) {
        for (Account account : accounts) {
            if (account.getFirstName().equalsIgnoreCase(firstName) &&
                    account.getLastName().equalsIgnoreCase(lastName)) {
                return account;
            }
        }
        return null;
    }

    public static TransferResult transfer(String fromAccountNumber, String toAccountNumber, int sum) {
        sum = Math.abs(sum);
        Account fromAccount = findAccount(fromAccountNumber);
        if (fromAccount == null) {
            return new TransferResult(false, "Maksja kontot ei leitud.");
        }

        Account toAccount = findAccount(toAccountNumber);
        if (toAccount == null) {
            return new TransferResult(false, "Saaja kontot ei leitud.");
        }

        if (fromAccount.getBalance() >= sum) {
            // Teeme ülekande...
            fromAccount.setBalance(fromAccount.getBalance() - sum);
            toAccount.setBalance(toAccount.getBalance() + sum);
            return new TransferResult(true, "OK", fromAccount, toAccount);
        } else {
            return new TransferResult(false, "Kontol pole piisavalt vahendeid.", fromAccount, toAccount);
        }
    }
}
