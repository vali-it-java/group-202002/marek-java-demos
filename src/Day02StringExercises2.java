import java.util.Scanner;

public class Day02StringExercises2 {
    public static void main(String[] args) {
        System.out.println("Ex 1");
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";

        StringBuilder sb = new StringBuilder();
        sb.append(president1)
                .append(", ").append(president2)
                .append(", ").append(president3)
                .append(", ").append(president4)
                .append(", ").append(president5)
                .append(" on Eesti presidendid.");
        System.out.println(sb.toString());

        System.out.println("Ex 2");
        Scanner s = new Scanner("Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.");
        s.useDelimiter("Rida: ");
        System.out.println(s.next());
        System.out.println(s.next());
        System.out.println(s.next());
    }
}
