public class Day02Exercises {

    public static void main(String[] args) {

        // Ex 1
        float myFloat = 456.78F;
        double myDouble = 456.78;
        double myExtremelyLargeDouble = 3_432_569.56;

        String myText = "test";
        System.out.println(myText);

        boolean myBooleanValue = true;
        System.out.println(myBooleanValue);

        char myChar = 'a';
        String myString = "a";
        String myString2 = new String("a");
        short myShort = 'a';
        System.out.println(myShort);
        System.out.println(myChar);

        int[] myIntegers = {5, 91, 304, 405};
        System.out.println(myIntegers[2]);

        double[] myDecimals = {56.7, 45.8, 91.2};

        System.out.println("Ex 2");
        String city = "Berlin";
        if (city.equals("Milano")) {
            System.out.println("Ilm on soe.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam!");
        }

        System.out.println("Ex 3");
        int grade = 4;
        if (grade == 5) {
            System.out.println("Väga hea");
        } else if (grade == 4) {
            System.out.println("Hea");
        } else if (grade == 3) {
            System.out.println("Rahuldav");
        } else if (grade == 2) {
            System.out.println("Mitterahuldav");
        } else if (grade == 1) {
            System.out.println("Nõrk");
        } else {
            System.out.println("Ebakorrektne hinne");
        }

        System.out.println("Ex 4");
        switch (grade) {
            case 5:
                System.out.println("Väga hea");
                break;
            case 4:
                System.out.println("Hea");
                break;
            case 3:
                System.out.println("Rahuldav");
                break;
            case 2:
                System.out.println("Mitterahuldav");
                break;
            default:
                System.out.println("Nõrk");
        }

        switch (grade) {
            case 5:
            case 4:
            case 3:
                System.out.println("PASS");
                break;
            default:
                System.out.println("FAIL");
        }

        Grade myGrade = Grade.parse(Integer.parseInt(args[0]));
        System.out.println(myGrade);

        System.out.println("Ex 5");
        int age = 156;
        String ageDesc = (age > 100) ? "Vana" : "Noor";
        System.out.println("Kas noor või vana? " + ageDesc);

//        if (age > 100) {
//            ageDesc = "Vana";
//        } else {
//            ageDesc = "Noor";
//        }

        System.out.println("Ex 6");
        age = 99;
        ageDesc = (age > 100) ?
                ("Vana")
                :
                ((age == 100) ? "Peaaegu vana" : "Noor");
        System.out.println("Kas noor või vana? " + ageDesc);


    }

    enum Grade {
        WEAK("Nõrk"),
        NONSATISFACTORY("Mitterahuldav"),
        SATISFACTORY("Rahuldav"),
        GOOD("Hea"),
        EXCELLENT("Väga hea");

        private final String text;

        Grade(final String text) {
            this.text = text;
        }

        public static Grade parse(int grade) {
            if (grade == 5) {
                return Grade.EXCELLENT;
            } else if (grade == 4) {
                return GOOD;
            } else if (grade == 3) {
                return SATISFACTORY;
            } else if (grade == 2) {
                return NONSATISFACTORY;
            } else if (grade == 1) {
                return WEAK;
            } else {
                throw new RuntimeException("Incorrect grade value!");
            }
        }

        @Override
        public String toString() {
            return this.text;
        }
    }
}
