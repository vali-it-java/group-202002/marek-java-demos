public class Day02String {
    public static final double VAT_RATE = 1.3;

    public static void main(String[] args) {
        String s1 = args[0];
        String s2 = args[1];
        boolean areParametersEqual = s1 == s2;
        System.out.println(areParametersEqual);
        s2 = s1;
        areParametersEqual = s1 == s2;
        System.out.println(areParametersEqual);

        String s3 = args[0];
        String s4 = args[1];
        System.out.println(s3.equals(s4));
        System.out.println(s3.equalsIgnoreCase(s4));

        String myGreeting1 = "Hello";
        String myGreeting2 = "Hello";
        System.out.println("Kas on võrdsed? " + (myGreeting1 == myGreeting2));

        String quote = "Isa ütles: \"Too puid!\"";
        String multilineText = "rida 1\n\trida 2\n\trida 3 \'tere\' 'tere'";
        System.out.println(quote);
        System.out.println(multilineText);

//        StringBuilder
//        StringBuffer
        StringBuilder sb = new StringBuilder();
        sb.append("See on tekstijupp 1.");
        sb.append("See on tekstijupp 2.");
        sb.append("See on tekstijupp 3.");
        sb.append(45764);
        System.out.println(sb.toString());

        String names = "Mari, Malle, Kalle, Kaur, Keldi";
        String[] namesArray = names.split(", ");
        System.out.println(namesArray[2]);

        String city = "New York";
        System.out.println("Kui pikk sa oled? " + city.length());
        System.out.println("Esimene täht: " + city.charAt(0));
        System.out.println("Viimane täht: " + city.charAt(city.length() - 1));
        System.out.println("+".repeat(25));

//        final String myConstant = "cccc";
//        myConstant = "dddd";

        System.out.println(VAT_RATE);
    }
}
