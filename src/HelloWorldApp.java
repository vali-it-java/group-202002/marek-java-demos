public class HelloWorldApp {

    public static void main(String[] args) {
        System.out.println("Tere tere, " + args[0]);

        Integer myInt = 654;
        System.out.println(myInt.byteValue());
        System.out.println(Integer.toBinaryString(myInt));

        // Kompilaator ignoreerib mind :(
        /*
            Kompilaator ei tee minust ka välja!

        */
    }
}
