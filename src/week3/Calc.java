package week3;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Calc {
    public static void main(String[] args) {
        String[] nums = { "888", "999" };

        List<Stack<Integer>> numList = new ArrayList<>();
        for (int i = 0; i < nums.length; i++) {
            Stack<Integer> nStack = new Stack<>();
            for (String c : nums[i].split("")) {
                nStack.add(Integer.parseInt(c));
            }
            numList.add(nStack);
        }

        String finalSum = "";
        int rem = 0;
        for (int i = 0; i < nums[0].length(); i++) {
            int sum = 0;
            for (Stack<Integer> numStack : numList) {
                sum = sum + numStack.pop();
            }
            int digit = (sum + rem) % 10;
            rem = (sum + rem) / 10;
            finalSum = digit + finalSum;
        }
        finalSum = (rem > 0 ? rem : "") + finalSum;

        System.out.println(finalSum);

    }

}
