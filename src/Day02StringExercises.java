public class Day02StringExercises {
    public static void main(String[] args) {
        System.out.println("Ex 1");
        System.out.println("Hello, World!");
        System.out.println("Hello, \"World\"!");
        System.out.println("Steven Hawking once said: \"Life would be tragic if it weren\'t funny.\"");
        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool  \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\"");
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");

        System.out.println("Ex 2");
        String tallinnPopulation = "450 000";
        System.out.println("Tallinnas elab " + tallinnPopulation + " inimest.");
        String text = String.format("Tallinnas elab %s inimest.", tallinnPopulation);
        System.out.println(text);
        System.out.printf("Tallinnas elab %s inimest.\n", tallinnPopulation);

        int populationOfTallinn = 450_000;
        System.out.printf("Tallinnas elab %,d inimest.\n", populationOfTallinn);

        System.out.println("Ex 3");
        String bookTitle = "Rehapapp";
        System.out.println("Raamatu \"" + bookTitle + "\" autor on Anderus Kivirähk.");

        System.out.println("Ex 4");
        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        String planetCount = "8";

        String text2 = planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 +
                ", " + planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 +
                " on Päikesesüsteemi " + planetCount + " planeeti.";
        System.out.println(text2);

        String text3 = String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %s planeeti.",
                planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);
        System.out.println(text3);
    }
}
